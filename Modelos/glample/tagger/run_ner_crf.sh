#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0 python train.py  --pre_emb "dataset/w2vec/nlx_best_dsm.w2v" \
 --train "dataset/harem/harem1-bilstm.bicnn.train" --dev "dataset/harem/harem1-bilstm.bicnn.dev" --test "dataset/harem/harem1-bilstm.bicnn.test"
