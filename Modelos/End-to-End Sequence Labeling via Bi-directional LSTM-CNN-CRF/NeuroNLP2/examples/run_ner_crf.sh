#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0 python NERCRF.py --mode LSTM --num_epochs 200 --batch_size 16 --hidden_size 256 --num_layers 1 \
 --char_dim 30 --num_filters 30 --tag_space 128 \
 --learning_rate 0.01 --decay_rate 0.05 --schedule 1 --gamma 0.0 \
 --dropout std --p_in 0.33 --p_rnn 0.33 0.5 --p_out 0.5 --unk_replace 0.0 --bigram \
 --embedding word2vec --embedding_dict "data/w2vec/nlx_best_dsm.w2v" \
 --train "data/harem-paramopama/harem1-paramopama-bilstm.bicnn.train" --dev "data/harem-paramopama/harem1-paramopama-bilstm.bicnn.dev" --test "data/harem-paramopama/harem1-paramopama-bilstm.bicnn.test"
