import re
from sklearn.model_selection import train_test_split
import numpy as np

def prepara_formato_bilstmcnn(ifile, ofile, add_index=True):
    with open(ifile, 'r', encoding='utf-8') as f:
        corpus_iobes = f.read()
        corpus_bilstmcnn = re.sub('(?P<inicio>.+\t)', '\g<inicio>NNP I-NP\t', corpus_iobes)
        corpus_bilstmcnn = re.sub('\t', ' ', corpus_bilstmcnn)
    with open(ofile, 'w', encoding='utf-8') as f:
        f.write(corpus_bilstmcnn)
    if add_index:
        add_starting_index(ofile, re.sub('\..+', '.bicnn', ofile))


def gera_train_test(ifile, test_size):
    with open(ifile, 'r', encoding='utf-8') as f:
        corpus_bicnn = f.read()
    exemplos = corpus_bicnn.split('\n\n')
    #train, test = train_test_split(exemplos, test_size=test_size)
    train, dev, test = np.split(exemplos, [int(.6 * len(exemplos)), int(.8 * len(exemplos))])
    with open(re.sub('\..+', '.bicnn.train', ifile), 'w', encoding='utf-8') as f:
        f.write('\n\n'.join(train))
    with open(re.sub('\..+', '.bicnn.dev', ifile), 'w', encoding='utf-8') as f:
        f.write('\n\n'.join(dev))        
    with open(re.sub('\..+', '.bicnn.test', ifile), 'w', encoding='utf-8') as f:
        f.write('\n\n'.join(test))

def add_starting_index(ifile, ofile):
    with open(ifile, 'r') as reader, open(ofile, 'w') as writer:
        prev = None
        skip_next = False
        for line in reader:
            if skip_next:
                skip_next = False
                continue
            line = line.strip()
            docstart = line.startswith('-DOCSTART-')
            if docstart:
                skip_next = True
            if len(line) == 0 or docstart:
                prev = None
                if not docstart:
                    writer.write('\n')
                continue

            tokens = line.split()

            if prev is None:
                prev = 1
            else:
                prev += 1

            indexed_tokens = [str(prev)] + tokens

            # print tokens
            writer.write(" ".join(indexed_tokens))
            writer.write('\n')
